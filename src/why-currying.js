import * as R from 'ramda';

const roles = [
  { name: 'admin', permissions: ['sessions:get', 'sessions:push', 'user:create'] },
  { name: 'user', permissions: ['sessions:get', 'sessions:push'] },
];


// so what makes currying useful?
// Composition!

const getAdminPermissions = R.pipe(
  R.find(R.propEq('name', 'admin')),
  R.prop('permissions')
);

const adminPermissions = getAdminPermissions(roles);
console.log('adminPermissions', adminPermissions); // => ['sessions:get', 'sessions:push', 'user:create']










/* 
  So how does this work?
  We're going to "pipe" roles through the chain of functions, taking each return value and
  calling the next function in the chain with each subsequent return value as the parameter.

  roles
  ------------------------------------------|
                                            v
    R.find((obj) => obj.role === 'admin', roles) // => capture this return value (in this case, `{ role: 'admin', ...}`)
    |
    --------------------------|
                              v
    R.path(['permissions'], role) // => grabs the "permissions" property (in this case, `['sessions:get', 'sessions:push', 'user:create']`)
    |
    --> ['sessions:get', 'sessions:push', 'user:create']
*/









// Written in the imperative style:
const getAdminPermissionsImperative = (roles) => {
  const adminRole = roles.find((role) => role.name === 'admin');
  if(!adminRole) {
    return undefined;
  }

  return adminRole.permissions;
}

// Ramda-style:
const getAdminPermissionsRamda = R.pipe(
  R.find(R.propEq('name', 'admin')),
  R.path(['permissions'])
);

















// Another example
const tasks = [
  {
    name: 'Buy food',
    complete: true,
    priority: 0,
    userId: '1'
  },
  {
    name: 'Cook food',
    complete: false,
    priority: 1,
    userId: '1'
  },
  {
    name: 'Eat food',
    complete: false,
    priority: 1,
    userId: '2'
  },
  {
    name: 'Take a nap',
    complete: false,
    priority: 2,
    userId: '2'
  },
  {
    name: 'Wake up',
    complete: false,
    priority: 2,
    userId: '2'
  },
  {
    name: 'Do dishes',
    complete: false,
    priority: 2,
    userId: '2'
  },
];

const getIncomplete = R.filter(R.propEq('complete', false));
const sortByPriority = R.sortBy(R.prop('priority'));
const groupByUser = R.groupBy(R.prop('userId'));

const prioritizeTasks = R.pipe(
  getIncomplete,
  sortByPriority,
  groupByUser
);

const prioritizedTasks = prioritizeTasks(tasks);
console.log('prioitizedTasks', prioritizedTasks);

// the above can be rewritten as:
const incompleteTasks = getIncomplete(tasks);
const sortedIncompleteTasks = sortByPriority(incompleteTasks);
const prioritizedTasks2 = groupByUser(sortedIncompleteTasks);

















// we can go further though...
const topTwoTasks = R.map(R.take(2), prioritizeTasks(tasks));
console.log('topTwoTasks', topTwoTasks);