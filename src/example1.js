import * as R from 'ramda';
import _ from 'lodash';

const users = [
  {
    name: 'George',
    role: 'admin',
  },
  {
    name: 'Jim',
    role: 'user',
  },
  {
    name: 'Geraldine',
    role: 'facility_admin',
  },
  {
    name: 'Gina',
    role: 'user',
  },
  {
    name: 'Courtney',
    role: 'user',
  },
];

// lame lodash example
const findAdminGs = (users) => {
  return _.filter(users, (user) => {
    return user.name.includes('G') && _.includes(['facility_admin', 'admin'], user.role);
  });
};

console.log('adminGs: ', findAdminGs(users));

// fancy Ramda example
const findAdminGs2 = R.filter(
  R.where({
    name: R.includes('G'),
    role: (role) => R.includes(role, ['facility_admin', 'admin']),
  }),
);

console.log('adminGs2', findAdminGs2(users));
