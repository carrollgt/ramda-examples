import _ from 'lodash';
import * as R from 'ramda';

const targetObject = { propName: 'test' };
// notice the data is passed _last_
R.prop('propName', targetObject); // => 'test'

// notice we only pass one parameter here
const getFoo = R.prop('foo');

// getFoo is a function because we only passed one parameter - Ramda "curries" it for us
console.log('foo: ', getFoo({ foo: 'bar' })); // => 'bar'

// here's an example with lists using R.map
const foos = [{ foo: 'bar' }, { foo: 'buzz' }, { foo: 'baz' }];
R.map(getFoo, foos); // => ['bar', 'buzz', 'baz']

// Every function in Ramda is auto-curried, so we can do the same with `R.map`
const mapToFoos = R.map(getFoo);
mapToFoos(foos); // => ['bar', 'buzz', 'baz']


// curried
const prop = (fieldName) => {
  return (obj) => {
    return obj[fieldName];
  };
};

// very naiive
const autoCurriedProp = (fieldName, obj) => {
  if(obj === undefined) {
    return (obj) => obj[fieldName];
  }

  return obj[fieldName];
}





















const getPrefix = (words) => {
  return _.map(words, (word) => {
    return _.take(word, 4);
  });
};

console.log('prefix', getPrefix(['BASE:12340-1', 'UUID:14222kasdfa']));
// => [ 'BASE', 'UUID' ]


